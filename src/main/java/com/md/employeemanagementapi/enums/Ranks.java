package com.md.employeemanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Ranks {
    CONTRACT_WORKER("계약직", true),
    FULL_TIME("정직원", false),
    VICE_PRESIDENT("부사장", false),
    PRESIDENT("사장", false),
    EXECUTIVE("임원", false   );

    private final String ranksName;
    private final Boolean isContract;

}
