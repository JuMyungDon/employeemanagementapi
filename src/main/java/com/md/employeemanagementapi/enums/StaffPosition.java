package com.md.employeemanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StaffPosition {
    MEMBER("팀원"),
    MANAGER("매니"),
    BRANCH_MANAGER("지점장"),
    DIRECTOR_OF_THE_CEMTER("센터장"),
    DIRECTOR("원장");

    private final String positionName;
}