package com.md.employeemanagementapi.service;

import com.md.employeemanagementapi.entity.Employee;
import com.md.employeemanagementapi.model.EmployeeItem;
import com.md.employeemanagementapi.model.EmployeeRequest;
import com.md.employeemanagementapi.model.EmployeeResponse;
import com.md.employeemanagementapi.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest request) {
        Employee addData = new Employee();
        addData.setName(request.getName());
        addData.setJoiningDate(LocalDate.now());
        addData.setCommute(request.getCommute());
        addData.setBrithDay(request.getBrithDay());
        addData.setStaffPosition(request.getStaffPosition());
        addData.setRanks(request.getRanks());
        addData.setEndDate(LocalDate.now());
        addData.setStaffNumber(request.getStaffNumber());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setEmail(request.getEmail());
        addData.setMemo(request.getMemo());

        employeeRepository.save(addData);
    }

    public List getEmpoyees(){
        List<Employee> originList = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        for (Employee employee : originList) {
            EmployeeItem addItem = new EmployeeItem();
            addItem.setId(employee.getId());
            addItem.setName(employee.getName());
            addItem.setJoiningDate(employee.getJoiningDate());
            addItem.setCommute(employee.getCommute());
            addItem.setBrithDay(employee.getBrithDay());
            addItem.setStaffPosition(employee.getStaffPosition().getPositionName());
            addItem.setRanks(employee.getRanks().getRanksName());
            addItem.setEndDate(employee.getEndDate());
            addItem.setStaffNumber(employee.getStaffNumber());
            addItem.setPhoneNumber(employee.getPhoneNumber());
            addItem.setAddress(employee.getAddress());
            addItem.setEmail(employee.getEmail());

            result.add(addItem);
        }

        return result;
    }

    public EmployeeResponse getEmployee(long id) {
        Employee originData = employeeRepository.findById(id).orElseThrow();

        EmployeeResponse response = new EmployeeResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setJoiningDate(originData.getJoiningDate());
        response.setCommute(originData.getCommute());
        response.setBrithDay(originData.getBrithDay());
        response.setStaffPositionName(originData.getStaffPosition().getPositionName());
        response.setRanksName(originData.getRanks().getRanksName());
        response.setIsContract(originData.getRanks().getIsContract() ? "예" : "아니오");
        response.setEndDate(originData.getEndDate());
        response.setStaffNumber(originData.getStaffNumber());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setEmail(originData.getEmail());
        response.setMemo(originData.getMemo());
        Boolean isContract;
        return response;
    }
}
