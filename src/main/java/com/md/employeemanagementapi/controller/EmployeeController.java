package com.md.employeemanagementapi.controller;

import com.md.employeemanagementapi.model.EmployeeItem;
import com.md.employeemanagementapi.model.EmployeeRequest;
import com.md.employeemanagementapi.model.EmployeeResponse;
import com.md.employeemanagementapi.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/people")
    public String setEmployee(@RequestBody EmployeeRequest request) {
        employeeService.setEmployee(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<EmployeeItem> getEmployees() {
        return employeeService.getEmpoyees();
    }

    @GetMapping("/detail/{id}")
    public EmployeeResponse getEmployee(@PathVariable long id) {
        return employeeService.getEmployee(id);
    }
}
