package com.md.employeemanagementapi.entity;

import com.md.employeemanagementapi.enums.Ranks;
import com.md.employeemanagementapi.enums.StaffPosition;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDate joiningDate;

    @Column(nullable = false)
    private String commute;

    @Column(nullable = false)
    private String brithDay;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StaffPosition staffPosition;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Ranks ranks;

    private LocalDate endDate;

    @Column(nullable = false)
    private Long staffNumber;

    @Column(nullable = false, length = 15)
    private String phoneNumber;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String email;

    @Column(columnDefinition = "TEXT")
    private String memo;
}
