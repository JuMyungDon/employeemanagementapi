package com.md.employeemanagementapi.model;

import com.md.employeemanagementapi.enums.Ranks;
import com.md.employeemanagementapi.enums.StaffPosition;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeResponse {
    private Long id;
    private String name;
    private LocalDate joiningDate;
    private String commute;
    private String brithDay;
    private String staffPositionName;
    private String ranksName;
    private String isContract;
    private LocalDate endDate;
    private Long staffNumber;
    private String phoneNumber;
    private String address;
    private String email;
    private String memo;
}
