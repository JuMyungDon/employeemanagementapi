package com.md.employeemanagementapi.model;

import com.md.employeemanagementapi.enums.Ranks;
import com.md.employeemanagementapi.enums.StaffPosition;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeItem {
    private Long id;
    private String name;
    private LocalDate joiningDate;
    private String commute;
    private String brithDay;
    private String staffPosition;
    private String ranks;
    private LocalDate endDate;
    private Long staffNumber;
    private String phoneNumber;
    private String address;
    private String email;
}
