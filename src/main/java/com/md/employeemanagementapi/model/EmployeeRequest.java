package com.md.employeemanagementapi.model;

import com.md.employeemanagementapi.enums.Ranks;
import com.md.employeemanagementapi.enums.StaffPosition;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeRequest {
    private String name;
    private String commute;
    private String brithDay;
    @Enumerated(value = EnumType.STRING)
    private StaffPosition staffPosition;
    @Enumerated(value = EnumType.STRING)
    private Ranks ranks;
    private Long staffNumber;
    private String phoneNumber;
    private String address;
    private String email;
    private String memo;
}